import Info from 'components/info/Info';
import React from 'react';

import './app.scss';
import UsersListWrap from './wraps/users-list-wrap/UsersListWrap';

function App() {
  return (
    <div className="app">
      <header>
        <Info/>
      </header>
      <div className="container">
        <UsersListWrap />
      </div>
      <footer>
        <Info/>
      </footer>
    </div>
  );
}

export default App;
