import React from 'react';
import UserDeleteCommentBtnWrap from 'wraps/user-btn-wraps/UserDeleteCommentBtnWrap';
import { UserType } from '../../types/user-type';
import UserInputCommentWrap from '../user-comment-input-wrap/UserInputCommentWrap';
import './user-comment-wrap.scss';

type PropsType = {
    user: UserType,
    index: number
}

export default function UserCommentWrap(props: PropsType) {


    return (
        <div className='user-comment'>

            <UserDeleteCommentBtnWrap index={props.index} disabled={!!!props.user.comment}/>

            {
                !props.user.active && props.user.comment &&
                <p className="user-comment__text">{props.user.comment}</p>
            }

            {
                !props.user.active && !props.user.comment &&
                <p className="user-comment__text"><i>no comments ...</i></p>
            }

            {
                props.user.active &&
                <UserInputCommentWrap value={props.user.comment} index={props.index}/>
            }
        </div>
    )
}
