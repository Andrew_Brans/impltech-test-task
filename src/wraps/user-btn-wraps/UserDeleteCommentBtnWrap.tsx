import React from 'react';
import { useDispatch } from 'react-redux';
import UserBtn from '../../components/user-btn/UserBtn';
import letChangeUserCommentAction from '../../store/actions/users/let-change-user-comment';

type PropsType = {
    index: number,
    disabled: boolean,

}
export default function UserDeleteCommentBtnWrap(props: PropsType) {
    const dispatch = useDispatch()
    let delComment = () => {
        dispatch(letChangeUserCommentAction(props.index, ""));
    }

    return (
        <UserBtn text='delete commit' onClick={delComment} color="danger" disabled={props.disabled}/>
    )
}
