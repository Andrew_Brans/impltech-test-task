import React from 'react';
import { useDispatch } from 'react-redux';
import UserBtn from '../../components/user-btn/UserBtn';
import letActivateUser from '../../store/actions/users/let-activate-user';
import letChangeUserCommentAction from '../../store/actions/users/let-change-user-comment';

type PropsType = {
    index: number, 
    comment: string
}
export default function UserSaveCommentBtnWrap(props: PropsType) {
    const dispatch = useDispatch()
    let saveComment = () => {
        dispatch(letChangeUserCommentAction(props.index, props.comment));
        dispatch(letActivateUser(props.index))
    }

    return (
        <UserBtn text='save' onClick={saveComment}/>
    )
}
