import React from 'react';
import { useDispatch } from 'react-redux';
import UserBtn from '../../components/user-btn/UserBtn';
import letActivateUser from '../../store/actions/users/let-activate-user';
import { UserType } from '../../types/user-type';

type PropsType = {
    index: number,
    user: UserType,
}
export default function UserAddCommentBtnWrap(props: PropsType) {
    const dispatch = useDispatch();

    let text = props.user.comment ? 'edit commit' : 'add commit';
    let color = props.user.comment ? 'warning' : '';
    let addNewComment = () => {
        dispatch(letActivateUser(props.index))
    }
    return (
        <UserBtn text={text} onClick={addNewComment} color={color}/>
    )
}
