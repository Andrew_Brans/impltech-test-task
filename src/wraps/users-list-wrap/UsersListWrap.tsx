import React, { useEffect } from 'react'
import { useDispatch, useSelector} from 'react-redux';
import UserItem from '../../components/user-item/UserItem';
import { appStateType } from '../../store/store';
import { fetchUsersThunk } from '../../store/thunks/fetch-users';
import './users-list-wrap.scss';

export default function UsersListWrap() {
    const dispatch = useDispatch();
    const users = useSelector((state: appStateType) => state.usersReducer.users);

    useEffect(() => {
        fetchUsersThunk(dispatch)
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <ul className="users-list">
           {
               users.map((u, i) => {
                   return (
                    <li key={u.id}>
                        <UserItem user={u} index={i}/>
                    </li>
                   )
               })
           }
        </ul>
    )
}
