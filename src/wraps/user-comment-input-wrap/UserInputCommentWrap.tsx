import React, { useEffect, useState } from 'react';
import UserSaveCommentBtnWrap from '../user-btn-wraps/UserSaveCommentBtnWrap';
import './user-input-comment-wrap.scss';

type PropsType = {
    value: string;
    index: number;
}

export default function UserInputCommentWrap(props: PropsType) {
    const [val, setVal] = useState("");

    useEffect(() => {
        setVal(() => {
            return props.value
        })
    }, [props.value]);

    let onChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        e.stopPropagation();
        e.preventDefault()

        setVal(() => {
            return e.target.value
        })
    }

    return (
        <div className='user-form-wrap'>
            <form className="user-form">
                <textarea value={val} className='user-input' onChange={(e) => onChange(e)} />
            </form>
            <div className="user-form-wrap__btn">
                <UserSaveCommentBtnWrap index={props.index} comment={val}></UserSaveCommentBtnWrap>
            </div>
           
        </div>
    )
}
