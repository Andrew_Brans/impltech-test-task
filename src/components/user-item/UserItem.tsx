import React from 'react';
import { UserType } from '../../types/user-type';
import UserAddCommentBtnWrap from '../../wraps/user-btn-wraps/UserAddCommentBtnWrap';
import UserCommentWrap from '../../wraps/user-comment-wrap/UserCommentWrap';
import './user-item.scss'

type PropsType = {
    user: UserType,
    index: number,
}

export default function UserItem(props: PropsType) {
    let activeClass = props.user.active? ' active' : ''
    return (
    <div className={`user-item${activeClass}`} >
        <div className="user-item__col user-item__col--id">{props.user.id}</div>
        <div className="user-item__col user-item__col--name">{props.user.name}</div>
        <div className="user-item__col user-item__col--birth">{props.user.birth_year}</div>
        <div className="user-item__col user-item__col--add-comment">
            <UserAddCommentBtnWrap index={props.index} user={props.user}/>
        </div>
        <div className="user-item__col user-item__col--comment">
            <UserCommentWrap user={props.user} index={props.index}/>
        </div>
    </div>
    )
}
