import React from 'react';
import './info.scss'

export default function Info() {
    return (
        <div className="info">
            <p>Andrey</p>
            <p><b>email: </b>branspost@gmail.com</p>
            <p><b>phone: </b>+38 098 126 31 28</p>
            <p><b>telegram: </b>@brans80</p>
        </div>
    )
}
