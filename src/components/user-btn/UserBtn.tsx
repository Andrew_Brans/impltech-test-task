import React from 'react';
import './user-btn.scss';

type PropsType = {
    onClick?: () => any,
    text: string,
    disabled?: boolean,
    color?: string,
}

export default function Button(props: PropsType) {
    let styleClass = props.color ? ' btn--'+props.color : ''
    let onClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        e.preventDefault();
        e.stopPropagation();

        if(props.onClick) {
            props.onClick()
        } else {
            return false
        }
    }
    return (
        <button disabled={props.disabled} className={'btn'+ styleClass} type='button' onClick={(e) => onClick(e)}>
            {props.text}
        </button>
    )
}
