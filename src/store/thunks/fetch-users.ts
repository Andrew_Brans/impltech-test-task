
import { FetchedUserType, UserType } from "../../types/user-type";
import letChangeUsersAction from "../actions/users/let-change-users";
import { appDispatchType } from "../store";

let fetchUsersHelper = () => {
    return fetch('https://swapi.dev/api/people/').then((res) => {
        return res.json()
    })
}

let convertFetchedUsersToUsers = (fetcedUsers: FetchedUserType[]): UserType[] => {
    return fetcedUsers.map((fU, i) => {
        return {...fU, id: i, comment: '', active: false}
    })
}


export let fetchUsersThunk = async (dispatch: appDispatchType) => {
    let fetchResult = await fetchUsersHelper().catch((error) => {
        console.log(error)
    })

    if(fetchResult) {
        let users: FetchedUserType[] = fetchResult.results;
        dispatch  (letChangeUsersAction(convertFetchedUsersToUsers(users)));
    }
}