import {
  UserType
} from "../../types/user-type"
import {
  LET_ACTIVATE_USER,
  LET_CHANGE_USERS,
  LET_CHANGE_USER_COMMENT,
  usersActionTypes
} from "../actions/users/users-action-constants"

export type usersRdcType = {
  users: UserType[]
}

const initState = (): usersRdcType => {
  return {
    users: []
  }
}

let usersReducer = (state = initState(), action: usersActionTypes) => {
  switch (action.type) {

    case LET_CHANGE_USERS: {
      const users = action.users
      return {
        ...state,
        users
      }
    }

    case LET_ACTIVATE_USER: {
      const users = action.users
      return {
        ...state,
        users
      }
    }

    case LET_CHANGE_USER_COMMENT: {
      const users = action.users
      return {
        ...state,
        users
      }
    }

    default:
      return state
  }
}

export default usersReducer