import { UserType } from "../../../types/user-type";

export const LET_CHANGE_USER_COMMENT = "LET_CHANGE_USER_COMMENT";
export const LET_CHANGE_USERS = "LET_CHANGE_USERS";
export const LET_ACTIVATE_USER = "LET_ACTIVATE_USER";

type letChangeUsersActionType = {
  type: typeof LET_CHANGE_USERS,
  users: UserType[],
};

type letChangeUserCommentActionType = {
  type: typeof LET_CHANGE_USER_COMMENT,
  users: UserType[],
};

type letActivateUserActionType = {
  type: typeof LET_ACTIVATE_USER,
  users: UserType[],
};

export type usersActionTypes = letChangeUsersActionType | letActivateUserActionType | letChangeUserCommentActionType
