import store from "../../store";
import { LET_ACTIVATE_USER, usersActionTypes } from "./users-action-constants"

const letActivateUser = (userIndex: number): usersActionTypes => {
    let users = [...store.getState().usersReducer.users];

    users[userIndex] = {...users[userIndex], active: !users[userIndex].active}

    let action: usersActionTypes = {
      type: LET_ACTIVATE_USER,
      users
    }

    return action
  }
  
  export default letActivateUser