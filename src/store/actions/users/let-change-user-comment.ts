import store from "../../store"
import { LET_CHANGE_USER_COMMENT, usersActionTypes } from "./users-action-constants"


const letChangeUserCommentAction = (userIndex: number, comment: string): usersActionTypes => {
    let users = [...store.getState().usersReducer.users];

    users[userIndex] = {...users[userIndex], comment}

    let action: usersActionTypes = {
      type: LET_CHANGE_USER_COMMENT,
      users
    }

    return action
  }
  
  export default letChangeUserCommentAction